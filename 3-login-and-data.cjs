const fs = require('fs')
const path = require('path')
const lipsumPath = path.join(__dirname, './lipsum.txt')
const newLipsumPath = path.join(__dirname, './newLipsum.txt')



// NOTE: Do not change the name of this file

// NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


function problem1() {

    const fileNames = Array(2).fill(0).map((element, index) => {
        return `./file${element + index + 1}.txt`
    })

    let createCheckList = fileNames.map(fileName => {
        return fs.promises.writeFile(path.join(__dirname, fileName), `I am a ${fileName}`)
    })

    Promise.all(createCheckList)
        .then(() => {
            console.log("All files are successfully created")
        })

        .catch(err => {
            console.error(err)
        })

    setTimeout(() => {

        let deleteCheckList = fileNames.map(fileName => {
            return fs.promises.unlink(path.join(__dirname, fileName))
        })

        Promise.all(deleteCheckList)
            .then(() => {
                console.log("All files are successfully deleted")
            })

            .catch(err => {
                console.error(err)
            })
    }, 2 * 1000)
}

// problem1()

// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining


function problem2() {

    fs.promises.readFile(lipsumPath)

        .then(lipsumData => {
            return fs.promises.writeFile(newLipsumPath, lipsumData)
        })

        .then(() => {
            return fs.promises.unlink(lipsumPath)
        })

        .catch(err => {
            console.error(err)
        })

}

// problem2()

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

// A. login with value 3 and call getData once login is successful

// function problem3() {

//     let id = 3
//     let name = "Test 3"

//     return login(name, id)

//         .then(() => {
//             return getData();
//         })

//         .then(data => {
//             console.log(data);
//         })
        
//         .catch(err => {
//             console.error(err);
//         })
// }
// problem3()